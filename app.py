# -*- coding: utf-8 -*-
#
# This file is part of Toolforge 404 handler
#
# Copyright (C) 2019 Wikimedia Foundation and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import logging
import os
import pwd
import socket
import urllib.parse
from typing import List, Tuple, Union

import cachelib
import flask
import fuzzywuzzy.process
import ldap3
import yaml
from werkzeug.routing import Rule


# Create the Flask application
app = flask.Flask(__name__)

# Load local config
__dir__ = os.path.dirname(__file__)
with open(os.path.join(__dir__, "config.yaml")) as f:
    app.config.update(yaml.safe_load(f))

logging.getLogger().addHandler(flask.logging.default_handler)

cache = cachelib.RedisCache(
    host=app.config["REDIS_HOST"],
    key_prefix=hashlib.sha256(
        "{0.pw_name}/{0.pw_dir}".format(pwd.getpwuid(os.getuid())).encode(
            "utf-8"
        )
    ).hexdigest(),
    socket_timeout=2,
)


def get_tool_data(name: str) -> Union[Tuple[List[str], bool], bool]:
    """Get the list of maintainers for a given tool.

    :param name: Tool name (e.g. "admin")
    :return: Tuple of (List of maintainer names (cn), Whether the tool is
      disabled or not), or False if tool does not exist
    """
    key = "tool-data:v1:{}".format(name)
    data = cache.get(key)
    if data is not None:
        return data

    with open("/etc/ldap.yaml") as f:
        cfg = yaml.safe_load(f)

    members = None
    disabled = None

    with ldap3.Connection(
        cfg["servers"], auto_bind=True, read_only=True
    ) as conn:
        # Some of the information we want is stored in the tool group
        # object (list of members), while some other information (tool
        # disabled status) is stored in the tool user object. So we query LDAP
        # for both, iterate over the results and pick the information that can
        # be retrieved from that object class.
        conn.search(
            f"ou=servicegroups,{cfg['basedn']}",
            f"(&(|(objectClass=groupOfNames)(objectClass=posixAccount))(cn={app.config['PROJECT']}.{name}))",  # noqa: E501
            attributes=["objectClass", "cn", "member", "pwdPolicySubentry"],
            size_limit=2,
            time_limit=5,
        )

        for tool in conn.entries:
            if "groupOfNames" in tool.objectClass:
                members_filter = "(|{})".format(
                    "".join([f"({m.split(',')[0]})" for m in tool.member])
                )
                r = conn.extend.standard.paged_search(
                    f"ou=people,{cfg['basedn']}",
                    members_filter,
                    attributes=["cn"],
                    paged_size=256,
                    time_limit=5,
                    generator=True,
                )
                members = [member["attributes"]["cn"][0] for member in r]
            elif "posixAccount" in tool.objectClass:
                disabled = (
                    f"cn=disabled,ou=ppolicies,{cfg['basedn']}"
                    in tool.pwdPolicySubentry
                )

    if members is not None and disabled is not None:
        data = (members, disabled)

    cache.set(key, data, timeout=300)
    return data


def get_all_tools():
    """Get a list of all tools from LDAP.

    :return: List of tools names (cn) with project prefix removed
    """
    key = "all_tools"
    data = cache.get(key)
    if data:
        return data

    with open("/etc/ldap.yaml") as f:
        cfg = yaml.safe_load(f)

    with ldap3.Connection(
        cfg["servers"], auto_bind=True, read_only=True
    ) as conn:
        r = conn.extend.standard.paged_search(
            "ou=servicegroups,{}".format(cfg["basedn"]),
            "(&(objectClass=groupOfNames)(cn={}.*))".format(
                app.config["PROJECT"]
            ),
            attributes=["cn"],
            paged_size=256,
            time_limit=5,
            generator=True,
        )

    data = [tool["attributes"]["cn"][0].split(".")[1] for tool in r]
    cache.set(key, data, timeout=3600)
    return data


def get_all_projects():
    """Get a list of all projects from LDAP.

    :return: List of project names (cn) with "project-" prefix removed
    """
    key = "all_projects"
    data = cache.get(key)
    if data:
        return data

    with open("/etc/ldap.yaml") as f:
        cfg = yaml.safe_load(f)

    with ldap3.Connection(
        cfg["servers"], auto_bind=True, read_only=True
    ) as conn:
        r = conn.extend.standard.paged_search(
            "ou=groups,{}".format(cfg["basedn"]),
            "(&(objectClass=groupOfNames)(cn=project-*))",
            attributes=["cn"],
            paged_size=256,
            time_limit=5,
            generator=True,
        )

    data = [proj["attributes"]["cn"][0].split("-", 1)[1] for proj in r]
    cache.set(key, data, timeout=3600)
    return data


def context_for_url(url, headers):
    """Generate a template context for the given URL and headers.

    Look at the incoming URL and decide if this was a request for an existing
    tool that is not currently running a webservice or just a random
    unrouted URL. In either case return a dict of information for building an
    error page including the status code that the ressponse should use.
    """
    url_parts = urllib.parse.urlparse(url)
    ctx = {
        "url": url,
        "path": url_parts.path,
        "tool": None,
        "maintainers": None,
        "has_ingress": "X-Ingress-Name" in headers,
        "toolsadmin_url": app.config["TOOLSADMIN_URL"],
        "toolhub_url": app.config["TOOLHUB_URL"],
        "wiki_url": app.config["WIKI_URL"],
        "status_code": 404,
    }

    try:
        ctx["tool"] = url_parts.hostname.split(".")[0]
    except IndexError:
        logging.exception(
            "Failed to extract tool name from %s", url_parts.hostname
        )
        ctx["status_code"] = 500
        return ctx

    try:
        tool_data = get_tool_data(ctx["tool"])
    except:
        logging.exception(
            "Failed to fetch maintainers for tool %s", ctx["tool"]
        )
        ctx["status_code"] = 500
        return ctx

    if tool_data:
        ctx["maintainers"], disabled = tool_data
        upstream_status = headers.get("X-Code", "503")
        if upstream_status in ("502", "504"):
            ctx["status_code"] = int(upstream_status)
        elif not ctx["has_ingress"] and disabled:
            ctx["status_code"] = 410

    return ctx


def render_error_template(ctx):
    """Render an error response for the given context."""
    template = "{}.html".format(ctx["status_code"])
    return flask.render_template(template, **ctx), ctx["status_code"]


@app.context_processor
def inject_base_variables():
    return {
        "pod": socket.gethostname(),
    }


@app.route("/_/fourohfour-api/did-you-mean")
def did_you_mean():
    url_parts = urllib.parse.urlparse(flask.request.url)
    try:
        tool = url_parts.hostname.split(".")[0]
    except IndexError:
        logging.exception(
            "Failed to extract tool name from %s", url_parts.hostname
        )
        return flask.jsonify({}), 500

    try:
        did_you_mean = {
            "tools": fuzzywuzzy.process.extractBests(
                tool, get_all_tools(), score_cutoff=75, limit=5
            ),
            "projects": fuzzywuzzy.process.extractBests(
                tool, get_all_projects(), score_cutoff=75, limit=5
            ),
            "settings": {
                "toolsadminUrl": app.config["TOOLSADMIN_URL"],
                "openstackBrowserUrl": app.config["OPENSTACK_BROWSER_URL"],
            },
        }
    except:
        logging.exception("Failed to load did_you_mean info for %s", tool)
        return flask.jsonify({}), 500

    return flask.jsonify(did_you_mean)


@app.route("/_/fourohfour-healthz")
def healthz():
    return "OK"


@app.endpoint("index")
def index():
    """Process ?url=... requests"""
    if "url" in flask.request.args:
        ctx = context_for_url(flask.request.args["url"], flask.request.headers)
        return render_error_template(ctx)

    # ingress-nginx connection error pages
    if "X-Original-URI" in flask.request.headers:
        url = (
            flask.request.url_root.removesuffix("/")
            + flask.request.headers["X-Original-URI"]
        )
        ctx = context_for_url(url, flask.request.headers)
        return render_error_template(ctx)

    return error_404(None)


app.url_map.add(Rule("/", endpoint="index"))


@app.errorhandler(404)
def error_404(e):
    """Everything we do is 404 handling!

    Look at the incoming URL and decide if this was a request for an existing
    tool that is not currently running a webservice or just a random
    unrouted URL. In either case return an error page, but use a 503 status if
    the tool exists.
    """
    ctx = context_for_url(flask.request.url, flask.request.headers)
    return render_error_template(ctx)


if __name__ == "__main__":
    app.run()
