Toolforge 404 handler
=====================

Flask application to handle 404 routes for Toolforge.

Configuration
-------------

This tool expects a config.yaml file to be deployed in the same directory as
app.py. See example.config.yaml for a commented file that is suitable for use
on the primary Toolforge project. Toolsbeta will need adjustments, but that's
why there is a config file!

License
-------
[GPL-3.0-or-later](https://www.gnu.org/copyleft/gpl.html "GNU GPL 3.0 or later")
